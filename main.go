package main

import (
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/msvechla/rio-gameserver/internal/pkg/instrumentation"
	"gitlab.com/msvechla/rio-gameserver/internal/pkg/server"
)

var (
	flagAMQPURL     string
	flagAMQPUser    string
	flagAMQPPass    string
	flagRedisURL    string
	flagRedisPass   string
	flagSessionsURL string
)

func main() {
	log.SetFormatter(&log.JSONFormatter{})

	// initialize cli
	app := initCLI()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}

	// determine the dispatcherID
	hostname, err := os.Hostname()
	if err != nil {
		log.Fatalf("Error determining hostname: %s", err)
	}
	dispatcherID := fmt.Sprintf("gs-%s", hostname)

	// instrumentation
	mc := instrumentation.NewMetricsCollector(dispatcherID)
	log.Info("Serving prometheus metrics...")
	go mc.ServeMetrics()

	// create the dispatcher
	server, err := server.NewDispatcher(
		dispatcherID,
		mc,
		flagRedisURL,
		flagRedisPass,
		flagAMQPURL,
		flagAMQPUser,
		flagAMQPPass,
		flagSessionsURL,
	)
	if err != nil {
		log.Fatalf("Error creating dispatcher: %s", err)
	}

	// load state
	err = server.LoadFromState()
	if err != nil {
		log.Errorf("Error loading games from state: %s", err)
	} else {
		log.Info("Successfully loaded games from state")
	}

	server.BootstrapDispatcher()
}

// initCLI initializes the command line
func initCLI() *cli.App {
	app := &cli.App{
		Name:    "rio-gameserver",
		Usage:   "",
		Version: "v2.1.2",
	}

	generalFlags := []cli.Flag{
		&cli.StringFlag{
			Name:        "amqpURL",
			Value:       "localhost:5672/",
			Usage:       "AMQP URL to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_URL"},
			Destination: &flagAMQPURL,
		},
		&cli.StringFlag{
			Name:        "amqpUser",
			Value:       "user",
			Usage:       "AMQP user to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_USER"},
			Destination: &flagAMQPUser,
		},
		&cli.StringFlag{
			Name:        "amqpPass",
			Value:       "helloworld",
			Usage:       "AMQP Password to connect to RabbitMQ.",
			EnvVars:     []string{"AMQP_PASS"},
			Destination: &flagAMQPPass,
		},
		&cli.StringFlag{
			Name:        "redisURL",
			Value:       "localhost:6379",
			Usage:       "URL to connect to Redis.",
			EnvVars:     []string{"REDIS_URL"},
			Destination: &flagRedisURL,
		},
		&cli.StringFlag{
			Name:        "redisPass",
			Value:       "helloworld",
			Usage:       "Password to authenticate to Redis.",
			EnvVars:     []string{"REDIS_PASS"},
			Destination: &flagRedisPass,
		},
		&cli.StringFlag{
			Name:        "sessionsURL",
			Value:       "http://localhost:11111/sessions",
			Usage:       "Endpoint of the gio-sessions service.",
			EnvVars:     []string{"SESSIONS_URL"},
			Destination: &flagSessionsURL,
		},
	}

	app.Flags = append(app.Flags, generalFlags...)

	return app
}
