package rabbitmq

import (
	"fmt"
	"net/url"

	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"google.golang.org/protobuf/proto"
)

const (
	// ExchangeMsgFromGateway is the exchange for publishing messages from the gateway
	ExchangeMsgFromGateway = "MsgFromGateway"
	// ExchangeBroadcastMsgToGateway is the exchange where the gameservers publish messages for the gateway
	ExchangeBroadcastMsgToGateway = "BroadcastMsgToGateway"
	// ExchangeOpenGames is the exchange where open games are distributed by dispatcherID
	ExchangeOpenGames = "ExchangeOpenGames"
	// QueueOpenGamesRandom is the workqueu where new games are published and picked up at random by any gameserver
	QueueOpenGamesRandom = "OpenGamesRandom"
)

// Client used to communicate with rabbitmq
type Client struct {
	ID             *string
	AMQPUrl        string
	Conn           *amqp.Connection
	Outgoing       *amqp.Channel
	Incoming       *amqp.Channel
	OpenGames      *amqp.Channel
	QueuesDeclared map[string]bool
}

// NewClient returns a new client object
func NewClient(id *string, amqpURL string, amqpUser string, amqpPass string) (*Client, error) {
	var qd = make(map[string]bool)

	url := fmt.Sprintf("amqp://%s:%s@%s", url.PathEscape(amqpUser), url.PathEscape(amqpPass), amqpURL)

	c := Client{ID: id, AMQPUrl: url, QueuesDeclared: qd}
	return &c, c.Init()
}

// Init initializes the client
func (c *Client) Init() error {
	var err error
	c.Conn, err = amqp.Dial(c.AMQPUrl)

	if err != nil {
		return errors.Wrap(err, "Error establishing AMQP connection")
	}

	// TODO: implememnt correct reconnect logic for rabbitmq
	go func() {
		log.Fatalf("RabbitMQ closed connection, restarting to reconnect: %s", <-c.Conn.NotifyClose(make(chan *amqp.Error)))
	}()

	c.Outgoing, err = c.NewChannel()
	if err != nil {
		return errors.Wrapf(err, "Error establishing AMQP channel Outgoing")
	}

	c.Incoming, err = c.NewChannel()
	if err != nil {
		return errors.Wrapf(err, "Error establishing AMQP channel Incoming")
	}

	c.OpenGames, err = c.NewChannel()
	if err != nil {
		return errors.Wrapf(err, "Error establishing AMQP channel OpenGames")
	}

	err = c.Outgoing.ExchangeDeclare(
		ExchangeMsgFromGateway, // name
		"direct",               // type
		true,                   // durable
		false,                  // auto-deleted
		false,                  // internal
		false,                  // no-wait
		nil,                    // arguments
	)
	if err != nil {
		return errors.Wrapf(err, "Error declaring exchange %s", ExchangeMsgFromGateway)
	}

	err = c.Outgoing.ExchangeDeclare(
		ExchangeBroadcastMsgToGateway, // name
		"direct",                      // type
		true,                          // durable
		false,                         // auto-deleted
		false,                         // internal
		false,                         // no-wait
		nil,                           // arguments
	)
	if err != nil {
		return errors.Wrapf(err, "Error declaring exchange %s", ExchangeBroadcastMsgToGateway)
	}

	err = c.Outgoing.ExchangeDeclare(
		ExchangeOpenGames, // name
		"direct",          // type
		true,              // durable
		false,             // auto-deleted
		false,             // internal
		false,             // no-wait
		nil,               // arguments
	)
	if err != nil {
		return errors.Wrapf(err, "Error declaring exchange %s", ExchangeOpenGames)
	}

	err = c.EnsureOpenGamesRandomQueueExists()
	if err != nil {
		return err
	}

	err = c.EnsureServerSpecificQueuesExist()
	if err != nil {
		return err
	}

	return nil
}

// EnsureOpenGamesRandomQueueExists declares the queue for open games waiting to be picked up by a gameserver
func (c *Client) EnsureOpenGamesRandomQueueExists() error {

	q, err := c.Incoming.QueueDeclare(
		QueueOpenGamesRandom, // name
		true,                 // durable
		false,                // delete when unused
		false,                // exclusive
		false,                // no-wait
		nil,                  // arguments
	)

	if err != nil {
		return errors.Wrapf(err, "Error declaring queue %s", QueueOpenGamesRandom)
	}
	log.Infof("Successfully declared queue %s", q.Name)

	return nil
}

// EnsureServerSpecificQueuesExist declares the main servers queue
func (c *Client) EnsureServerSpecificQueuesExist() error {

	q, err := c.Incoming.QueueDeclare(
		*c.ID, // name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)

	if err != nil {
		return errors.Wrapf(err, "Error declaring queue for serverID %s", *c.ID)
	}
	log.Infof("Successfully declared queue for serverID %s", q.Name)

	gq, err := c.Incoming.QueueDeclare(
		c.GetMyOpenGamesQueue(), // name
		true,                    // durable
		false,                   // delete when unused
		false,                   // exclusive
		false,                   // no-wait
		nil,                     // arguments
	)

	if err != nil {
		return errors.Wrapf(err, "Error declaring queue for serverID %s", *c.ID)
	}
	log.Infof("Successfully declared queue for my open games %s", gq.Name)

	err = c.Incoming.QueueBind(
		c.GetMyOpenGamesQueue(), // queue name
		*c.ID,                   // routing key
		ExchangeOpenGames,       // exchange
		false,
		nil)

	if err != nil {
		return errors.Wrapf(err, "Error binding queue %s to exchange %s via routing key for dispatcherID %s", c.GetMyOpenGamesQueue(), ExchangeOpenGames, *c.ID)
	}
	log.Infof("Successfully bound queue %s to exchange %s via %s", c.GetMyOpenGamesQueue(), ExchangeOpenGames, *c.ID)

	return nil
}

// GetMyOpenGamesQueue returns the name of the open games queue
func (c *Client) GetMyOpenGamesQueue() string {
	return fmt.Sprintf("OpenGames-%s", *c.ID)
}

// TODO: add mechanism for unbinding no longer managed games

// BindQueueForGameServer binds the servers queue for incoming messages for a specific gameID
func (c *Client) BindQueueForGameServer(gameID string) error {

	err := c.Incoming.QueueBind(
		*c.ID,                  // queue name
		gameID,                 // routing key
		ExchangeMsgFromGateway, // exchange
		false,
		nil)

	if err != nil {
		return errors.Wrapf(err, "Error binding queue %s to exchange %s via routing key for gameID %s", *c.ID, ExchangeMsgFromGateway, gameID)
	}

	log.Infof("Successfully bound queue %s to exchange %s via %s", gameID, ExchangeMsgFromGateway, gameID)
	return nil
}

// UnbindQueueForGameServer unbinds the servers queue for a specific gameID
func (c *Client) UnbindQueueForGameServer(gameID string) error {

	err := c.Incoming.QueueUnbind(*c.ID, gameID, ExchangeMsgFromGateway, nil)

	if err != nil {
		return errors.Wrapf(err, "Error unbinding queue %s from exchange %s via routing key for gameID %s", *c.ID, ExchangeMsgFromGateway, gameID)
	}

	log.Infof("Successfully unbound queue %s from exchange %s via %s", gameID, ExchangeMsgFromGateway, gameID)
	return nil
}

// NewChannel returns a new channel on the amqp connection
func (c *Client) NewChannel() (*amqp.Channel, error) {
	ch, err := c.Conn.Channel()
	if err != nil {
		return nil, errors.Wrap(err, "Error opening AMQP channel")
	}
	return ch, nil
}

// BroadcastMsgToGateway broadcasts a message for a game to the gateways
func (c *Client) BroadcastMsgToGateway(message *messaging.Container) error {
	body, err := proto.Marshal(message)
	if err != nil {
		return errors.Wrap(err, "Error marshaling protobuf message")
	}

	c.Outgoing.Publish(
		ExchangeBroadcastMsgToGateway, // exchange
		message.GameID,                // routing key
		false,                         // mandatory
		false,                         // immediate
		amqp.Publishing{
			ContentType: "application/protobuf",
			Body:        body,
			AppId:       *c.ID,
		})
	return nil
}
