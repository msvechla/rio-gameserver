package server

import (
	"net/http"
)

func (d *Dispatcher) routes() {
	d.router.HandleFunc("/health", d.handleHealth)
}

// ServeHTTP satisfies the http handler interface
func (d *Dispatcher) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	d.router.Use(d.metricsCollector.MuxProm.Middleware)
	d.router.ServeHTTP(w, r)
}

// handleHealth handles requests to the health endpoint
func (d *Dispatcher) handleHealth(w http.ResponseWriter, r *http.Request) {
	// TODO: add proper healthcheck
	w.WriteHeader(http.StatusOK)
}
