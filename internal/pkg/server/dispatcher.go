package server

import (
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/pkg/errors"
	log "github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/msvechla/rio-gameserver/internal/pkg/communication/rabbitmq"
	"gitlab.com/msvechla/rio-gameserver/internal/pkg/game"
	"gitlab.com/msvechla/rio-gameserver/internal/pkg/instrumentation"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/redis"
	"google.golang.org/protobuf/proto"
)

const (
	queryKeyUsername = "playerName"
	queryKeyHost     = "host"
	actionJoinGame   = "join"
	actionHostGame   = "host"
	tickerInterval   = time.Minute
)

// Dispatcher creates new game servers for open games
type Dispatcher struct {
	ID               string
	GameServers      map[string]*game.Server
	AMQPClient       *rabbitmq.Client
	RedisClient      *redis.Client
	OutgoingMessages chan *messaging.Container
	router           *mux.Router
	ticker           *time.Ticker
	metricsCollector *instrumentation.MetricsCollector
	sessionsURL      string
}

// NewDispatcher creates a new dispatcher object
func NewDispatcher(
	id string,
	metricsCollector *instrumentation.MetricsCollector,
	redisURL string,
	redisPass string,
	amqpURL string,
	amqpUser string,
	amqpPass string,
	sessionsURL string,
) (*Dispatcher, error) {
	amqpClient, err := rabbitmq.NewClient(&id, amqpURL, amqpUser, amqpPass)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating AMQP client")
	}
	log.Info("Successfully established AMQP connection")

	redisClient, err := redis.NewClient(redisURL, redisPass)
	if err != nil {
		return nil, errors.Wrap(err, "Error creating redis client")
	}
	log.Info("Successfully established redis connection")

	d := Dispatcher{
		ID:               id,
		GameServers:      make(map[string]*game.Server),
		AMQPClient:       amqpClient,
		RedisClient:      redisClient,
		OutgoingMessages: make(chan *messaging.Container),
		router:           mux.NewRouter(),
		ticker:           time.NewTicker(tickerInterval),
		metricsCollector: metricsCollector,
		sessionsURL:      sessionsURL,
	}

	d.routes()
	d.initState()

	return &d, nil
}

// BootstrapDispatcher starts the asynchronus goroutines for managing messages
func (d *Dispatcher) BootstrapDispatcher() {
	go d.handleIncomingMessages()
	go d.handleOutgoingMessages()
	go d.handleOpenCloseGames()
	http.ListenAndServe("0.0.0.0:22222", d)
}

// initState initializes the dispatchers state from redis
func (d *Dispatcher) initState() error {
	return d.RedisClient.InitActiveGamesForGameServer(d.ID)
}

// handleOutgoingMessages handles messages to the gateways
func (d *Dispatcher) handleOutgoingMessages() {
	log.Info("Starting to handle outgoing messages")
	for {
		select {
		case m := <-d.OutgoingMessages:
			err := d.AMQPClient.BroadcastMsgToGateway(m)
			if err != nil {
				log.Errorf("Error publishing message to gateway: %s", err)
			}
		}
	}
}

// handleOpenCloseGames listens for open games, dispatches new gameservers and closes them on idle timeout
func (d *Dispatcher) handleOpenCloseGames() {
	openGamesRandom, err := d.AMQPClient.OpenGames.Consume(
		rabbitmq.QueueOpenGamesRandom, // queue
		"",                            // consumer
		true,                          // auto ack
		false,                         // exclusive
		false,                         // no local
		false,                         // no wait
		nil,                           // args
	)
	if err != nil {
		log.Errorf("Error consuming from queue %s: %s", rabbitmq.QueueOpenGamesRandom, err)
	}

	myOpenGames, err := d.AMQPClient.OpenGames.Consume(
		d.AMQPClient.GetMyOpenGamesQueue(), // queue
		"",                                 // consumer
		true,                               // auto ack
		false,                              // exclusive
		false,                              // no local
		false,                              // no wait
		nil,                                // args
	)
	if err != nil {
		log.Errorf("Error consuming from queue %s: %s", d.AMQPClient.GetMyOpenGamesQueue(), err)
	}

	log.Info("Starting to handle open games")

	for {
		select {
		case random := <-openGamesRandom:
			d.handleOpenGame(random)
		case mine := <-myOpenGames:
			d.handleOpenGame(mine)
		case <-d.ticker.C:
			d.HandleIdleTimeoutCheck()
		}
	}
}

func (d *Dispatcher) handleOpenGame(m amqp.Delivery) {
	var openGame messaging.Container
	if err := proto.Unmarshal(m.Body, &openGame); err != nil {
		log.Errorf("Failed to parse open game message:", err)
	}

	if openGame.GameID == "" {
		log.Errorf("Invalid GameID for open game: %s", openGame.GameID)
		return
	}

	d.StartNewGameServer(
		openGame.GetOpenGame().GetGameTitle(),
		&openGame.GetOpenGame().CardDeck,
		openGame.GameID,
		false,
	)
}

func (d *Dispatcher) handleIncomingMessages() {
	msgs, err := d.AMQPClient.Incoming.Consume(
		*d.AMQPClient.ID, // queue
		"",               // consumer
		true,             // auto ack
		true,             // exclusive
		false,            // no local
		false,            // no wait
		nil,              // args
	)
	if err != nil {
		log.Errorf("Error consuming from queue %s: %s", *d.AMQPClient.ID, err)
	}

	log.Info("Starting to handle incoming messages")

	for {
		select {
		case m := <-msgs:
			var msg messaging.Container
			if err := proto.Unmarshal(m.Body, &msg); err != nil {
				log.Errorf("Failed to parse incoming message:", err)
				continue
			}

			gs, err := d.getGameServerByID(msg.GameID)
			if err != nil {
				log.Errorf("GameServer %s for incoming message not found", msg.GameID)
				continue
			}

			gs.IncomingMessage <- m.Body
		}
	}
}

// LoadFromState starts all persisted game servers from redis
func (d *Dispatcher) LoadFromState() error {
	gameIDs, err := d.RedisClient.GetGameServerHostingGamesForDispatcher(d.ID)
	if err != nil {
		return errors.Wrap(err, "Error getting GameServerHostingGamesForDispatcher while loading from state")
	}
	d.metricsCollector.DPActiveGamesTotal.Set(float64(len(gameIDs)))

	for _, gID := range gameIDs {
		gs := d.StartNewGameServer("", messaging.CardDeckType_DEFAULT.Enum(), gID, true)
		err := gs.LoadFromState()
		if err != nil {
			return errors.Wrap(err, "Error while loading game from state")
		}
	}

	return nil
}

// StartNewGameServer creates a new GameDispatcher and runs it in a new goroutine
func (d *Dispatcher) StartNewGameServer(
	title string,
	cardDeck *messaging.CardDeckType,
	overrideID string,
	loadedFromState bool,
) *game.Server {
	gs := game.NewServer(
		title,
		*cardDeck,
		overrideID,
		d.OutgoingMessages,
		d.RedisClient,
		d.metricsCollector,
		d.sessionsURL,
	)
	d.GameServers[gs.ID] = gs
	d.AMQPClient.BindQueueForGameServer(gs.ID)
	go gs.Run()

	if !loadedFromState {
		err := gs.PersistGameState()
		if err != nil {
			log.Errorf("Error persisting initial game state for game %s to redis: %s", gs.ID, err)
		}

		d.RedisClient.IncrementActiveGamesForGameServerByAmount(d.ID, 1)
		d.RedisClient.AddGameServerHostingGamesForDispatcher(d.ID, gs.ID)
		d.metricsCollector.DPActiveGamesTotal.Inc()
	}
	return gs
}

// getGameServerByID returns a gameserver by its id
func (d *Dispatcher) getGameServerByID(id string) (*game.Server, error) {
	if gs, exists := d.GameServers[id]; exists {
		return gs, nil
	}
	return nil, fmt.Errorf("GameServer with ID %s not found", id)
}

// HandleIdleTimeoutCheck checks game servers for idle timeout and shuts them down if applicable
func (d *Dispatcher) HandleIdleTimeoutCheck() {
	log.Info("Checking game servers for idle timeouts...")

	for gsID, gs := range d.GameServers {
		if gs.LastUpdatedTS.Add(game.IdleTimeout).Before(time.Now()) {
			reason := fmt.Sprintf("Game %s expired, last updated at %s", gs.ID, gs.LastUpdatedTS)
			log.Infof(reason)
			gs.Shutdown(reason)

			count, err := d.RedisClient.IncrementActiveGamesForGameServerByAmount(d.ID, -1)
			if err != nil {
				log.Errorf("Error decreasing active games for dispatcher: %s", err)
			} else {
				log.Infof("New active games count is %f", count)
			}

			err = d.RedisClient.RemoveGameServerHostingGamesForDispatcher(d.ID, gsID)
			if err != nil {
				log.Errorf("Error removing hosted gamserver from dispatcher: %s", err)
			}

			err = d.AMQPClient.UnbindQueueForGameServer(gs.ID)
			if err != nil {
				log.Errorf("Error unbiniding from amqp: %s", err)
			}

			d.metricsCollector.DPActiveGamesTotal.Dec()

			delete(d.GameServers, gsID)
		}
	}
}
