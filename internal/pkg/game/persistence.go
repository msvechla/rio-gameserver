package game

import (
	"strconv"
	"time"

	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	"gitlab.com/msvechla/rio-gameserver/internal/pkg/instrumentation"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
)

func (g *Server) persistNewPlayer(userID string, hosting bool) error {
	if hosting {
		return g.RedisClient.AddGameHostForGame(g.ID, userID)
	}
	return g.RedisClient.AddGamePlayerForGame(g.ID, userID)
}

func (g *Server) persistDisconnectedPlayer(userID string, hosting bool) error {
	if hosting {
		return g.RedisClient.RemoveGameHostForGame(g.ID, userID)
	}
	return g.RedisClient.RemoveGamePlayerForGame(g.ID, userID)
}

// PersistGameState persists the current game state to redis
func (g *Server) PersistGameState() error {
	state := map[string]interface{}{
		"Title":         g.Title,
		"CardDeck":      int(g.CardDeck),
		"Topic":         g.Topic,
		"TopicLink":     g.TopicLink,
		"State":         int(g.State),
		"Round":         g.Round,
		"StartTS":       g.StartTS.Format(TimeFormat),
		"LastUpdatedTS": g.LastUpdatedTS.Format(TimeFormat),
	}

	return g.RedisClient.SetGameStateForGame(g.ID, state)
}

// LoadFromState loads a game state from redis
func (g *Server) LoadFromState() error {
	players, err := g.RedisClient.GetGamePlayersForGame(g.ID)
	if err != nil {
		return err
	}
	g.metricsCollector.DPActiveUsersTotal.With(prometheus.Labels{instrumentation.LabelIsHost: "false"}).Add(float64(len(players)))

	hosts, err := g.RedisClient.GetGameHostsForGame(g.ID)
	if err != nil {
		return err
	}
	g.metricsCollector.DPActiveUsersTotal.With(prometheus.Labels{instrumentation.LabelIsHost: "true"}).Add(float64(len(hosts)))

	state, err := g.RedisClient.GetGameStateForGame(g.ID)
	if err != nil {
		return err
	}

	for _, p := range players {
		g.Players[p] = NewPlayer(p, false)
	}

	for _, p := range hosts {
		g.Players[p] = NewPlayer(p, true)
	}

	g.Title = state["Title"]
	intDeck, err := strconv.ParseInt(state["CardDeck"], 10, 32)
	if err != nil {
		return errors.Wrap(err, "parsing carddeck value")
	}
	g.CardDeck = messaging.CardDeckType(intDeck)
	g.Topic = state["Topic"]
	g.TopicLink = state["TopicLink"]

	intState, err := strconv.ParseInt(state["State"], 10, 32)
	if err != nil {
		return errors.Wrap(err, "parsing state value")
	}
	g.State = messaging.GameStatus(intState)

	round, err := strconv.Atoi(state["Round"])
	if err != nil {
		return err
	}
	g.Round = int32(round)

	g.StartTS, err = time.Parse(TimeFormat, state["StartTS"])
	if err != nil {
		return err
	}

	g.LastUpdatedTS, err = time.Parse(TimeFormat, state["LastUpdatedTS"])
	if err != nil {
		return err
	}

	return nil
}
