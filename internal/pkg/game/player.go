package game

import "gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"

// Player represents one connected client to a game server
type Player struct {
	ID     string
	Card   messaging.CardDeckCards
	IsHost bool
}

// NewPlayer returns a new player object
func NewPlayer(id string, isHost bool) *Player {
	return &Player{
		ID:     id,
		Card:   messaging.CardDeckCards_SPECIAL_NOTCHOSEN,
		IsHost: isHost,
	}
}
