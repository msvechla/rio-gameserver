package game

import (
	"bytes"
	"fmt"
	"net/http"
	"strings"

	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/internal/pkg/instrumentation"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"google.golang.org/protobuf/proto"
)

// Run orchestrates the main game logic via incoming messages
func (g *Server) Run() {
	g.Log.Info("Started listening for events")

	for {
		select {
		case <-g.shutdown:
			return

		case m := <-g.IncomingMessage:

			// unmarshal protobuf
			var msg messaging.Container
			if err := proto.Unmarshal(m, &msg); err != nil {
				log.Errorf("Error marshaling protobuf message: %s", err)
				continue
			}

			g.Log.Debugf("New message from %s: %v", msg.GetUserID(), msg)

			// handle the message based on its type
			switch msg.Msg.(type) {

			case *messaging.Container_Connection:
				g.metricsCollector.DPMessagesReceivedTotal.With(instrumentation.DPMessageTypeConnection).Inc()

				switch msg.GetConnection().GetState() {
				case messaging.Connection_DISCONNECTED:
					g.HandleDisconnect(msg.GetUserID(), "player disconnected")
				default:
					g.HandleNewPlayer(msg.GetUserID(), msg.GetConnection().GetHosting())
				}

			case *messaging.Container_PlayerUpdate:
				g.metricsCollector.DPMessagesReceivedTotal.With(instrumentation.DPMessageTypePlayerUpdate).Inc()
				g.HandleIncomingPlayerUpdate(&msg)

			case *messaging.Container_HostUpdate:
				g.metricsCollector.DPMessagesReceivedTotal.With(instrumentation.DPMessageTypeHostUpdate).Inc()
				g.HandleIncomingHostUpdate(&msg)

			case *messaging.Container_KickUser:
				g.metricsCollector.DPMessagesReceivedTotal.With(instrumentation.DPMessageTypeKickUser).Inc()
				g.HandleDisconnect(msg.GetKickUser().GetUserID(), "kicked by host")

			default:
				g.metricsCollector.DPMessagesReceivedTotal.With(instrumentation.DPMessageTypeUndefined).Inc()
				log.Errorf("Unable to determine message type for message: %s", m)
			}

		}
	}
}

// HandleNewPlayer creates a new player for the game and broadcasts a status update
func (g *Server) HandleNewPlayer(userID string, hosting bool) {

	if g.PlayerExists(userID) {
		g.Log.Infof("Player reconnected: %s", userID)
	} else {
		newPlayer := NewPlayer(userID, hosting)
		g.Players[newPlayer.ID] = newPlayer
		g.Log.Infof("New player joined: %s", newPlayer.ID)
		g.metricsCollector.DPActiveUsersTotal.With(prometheus.Labels{instrumentation.LabelIsHost: fmt.Sprintf("%t", hosting)}).Inc()

		err := g.persistNewPlayer(userID, hosting)
		if err != nil {
			g.Log.Errorf("Error persisting new player: %s", err)
		}
	}

	g.BroadcastAndPersistStatusUpdate()
}

// HandleIncomingPlayerUpdate handles an incoming update of a player
func (g *Server) HandleIncomingPlayerUpdate(msg *messaging.Container) {
	g.Log.Infof("Received message from player %s: %v", msg.GetUserID(), msg.GetPlayerUpdate())

	playerJoinedMajority := g.PlayerJoinedMajorityVote(msg)

	g.Players[msg.GetUserID()].Card = msg.GetPlayerUpdate().GetCard()
	g.UpdateGameStatus(playerJoinedMajority)
	g.BroadcastAndPersistStatusUpdate()
}

// HandleIncomingHostUpdate handles an imcoming update of a host
func (g *Server) HandleIncomingHostUpdate(msg *messaging.Container) {
	g.Log.Infof("Received message from host %s: %v", msg.GetUserID(), msg.GetHostUpdate())

	if g.Players[msg.GetUserID()].IsHost {
		if msg.GetHostUpdate().GetGameTitle() != "" {
			g.Title = msg.GetHostUpdate().GetGameTitle()
		}

		if msg.GetHostUpdate().GetGameStatus() == messaging.GameStatus_NEW || msg.GetHostUpdate().GetGameStatus() == messaging.GameStatus_RUNNING {
			g.State = msg.GetHostUpdate().GetGameStatus()
			g.Round++
			g.ResetPlayerCards()
		}

		if msg.GetHostUpdate().GetGameTopic() != g.Topic {
			g.ChangeTopic(msg.GetHostUpdate().GetGameTopic(), msg.GetHostUpdate().GetGameTopicLink())
			g.ResetPlayerCards()
		}
	}

	g.UpdateGameStatus(false)
	g.BroadcastAndPersistStatusUpdate()
}

// HandleDisconnect handles the disconnect event of a player
func (g *Server) HandleDisconnect(playerID string, reason string) error {
	g.Log.Infof("Removing player %s from game, reason: %s", playerID, reason)
	g.metricsCollector.DPActiveUsersTotal.With(prometheus.Labels{instrumentation.LabelIsHost: fmt.Sprintf("%t", g.Players[playerID].IsHost)}).Dec()

	err := g.persistDisconnectedPlayer(playerID, g.Players[playerID].IsHost)
	if err != nil {
		g.Log.Errorf("Error persisting disconnected player: %s", err)
	}
	delete(g.Players, playerID)

	g.BroadcastKickUser(playerID)
	g.BroadcastAndPersistStatusUpdate()

	err = g.invalidateTokenForUser(playerID)
	if err != nil {
		return errors.Wrap(err, "invalidating token for user")
	}

	return nil
}

// invalidateTokenForUser invalidates a token for a specific user
func (g *Server) invalidateTokenForUser(userID string) error {
	msg := messaging.Container{
		Msg: &messaging.Container_UserToken{
			UserToken: &messaging.UserToken{UserID: userID},
		},
	}

	body, err := proto.Marshal(&msg)
	if err != nil {
		return errors.Wrap(err, "marshaling invalidate token message")
	}

	resp, err := http.Post(fmt.Sprintf("%s/internal/token/delete/%s", g.sessionsURL, g.ID), "application/protobuf", bytes.NewReader(body))
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return errors.Wrapf(err, "HTTP Error invalidating token for user %s: %d", userID, resp.StatusCode)
	}

	return nil
}

// invalidateAllTokens invalidates all tokens for a game
func (g *Server) invalidateAllTokens() error {
	resp, err := http.Post(fmt.Sprintf("%s/internal/token/delete-all/%s", g.sessionsURL, g.ID), "application/json", strings.NewReader(""))
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("HTTP Error invalidating all tokens: %d", resp.StatusCode)
	}

	return nil
}
