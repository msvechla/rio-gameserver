package game

import (
	"net/url"
	"time"

	"github.com/google/uuid"
	"github.com/montanaflynn/stats"
	"github.com/prometheus/client_golang/prometheus"
	log "github.com/sirupsen/logrus"
	"gitlab.com/msvechla/rio-gameserver/internal/pkg/instrumentation"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/messaging"
	"gitlab.com/msvechla/rio-gameserver/pkg/communication/redis"
)

const (
	// TimeFormat is the time format used for storing game updates
	TimeFormat = "2006-01-02T15:04:05"
	// IdleTimeout is the timeout after which a game will get terminated automatically after inactivity
	IdleTimeout = time.Minute * 60
	blankLink   = "about:blank"
)

// Server represents an instance of a game
type Server struct {
	ID                    string
	Title                 string
	CardDeck              messaging.CardDeckType
	Topic                 string
	TopicLink             string
	Players               map[string]*Player
	State                 messaging.GameStatus
	Round                 int32
	HighlightedPlayers    []string
	MajorityJoinedPlayers []string
	MajorityVotedCard     messaging.CardDeckCards
	UnanimousVote         bool
	IncomingMessage       chan []byte
	OutgoingMessage       chan *messaging.Container
	RedisClient           *redis.Client
	Log                   *log.Entry
	StartTS               time.Time
	LastUpdatedTS         time.Time
	shutdown              chan bool
	metricsCollector      *instrumentation.MetricsCollector
	sessionsURL           string
}

// NewServer creates a new game server object
func NewServer(title string, cardDeck messaging.CardDeckType, overrideID string, outgoingMessage chan *messaging.Container, redisClient *redis.Client, metricsCollector *instrumentation.MetricsCollector, sessionsURL string) *Server {

	var id string
	if overrideID != "" {
		id = overrideID
	} else {
		id = uuid.New().String()
	}

	g := Server{
		ID:                    id,
		Title:                 title,
		CardDeck:              cardDeck,
		Players:               make(map[string]*Player),
		HighlightedPlayers:    []string{},
		MajorityJoinedPlayers: []string{},
		MajorityVotedCard:     messaging.CardDeckCards_SPECIAL_NOMAJORITY,
		UnanimousVote:         false,
		State:                 messaging.GameStatus_NEW,
		Round:                 0,
		IncomingMessage:       make(chan []byte),
		OutgoingMessage:       outgoingMessage,
		RedisClient:           redisClient,
		StartTS:               time.Now(),
		LastUpdatedTS:         time.Now(),
		shutdown:              make(chan bool),
		metricsCollector:      metricsCollector,
		sessionsURL:           sessionsURL,
	}

	g.Log = log.WithFields(log.Fields{
		"gameServerID": g.ID,
	})
	g.Log.Info("Created new server")
	return &g
}

// PlayerExists determines whether a player exists in the game
func (g *Server) PlayerExists(playerName string) bool {
	_, exists := g.Players[playerName]
	return exists
}

// PlayerJoinedMajorityVote checks whether a player joined the majority and updates the chosen card accordingly
func (g *Server) PlayerJoinedMajorityVote(msg *messaging.Container) bool {
	if g.majorityVoteExists() {
		if msg.GetPlayerUpdate().GetCard() == messaging.CardDeckCards_SPECIAL_JOINMAJORITY {
			// switch the chosen card from the indicator to the actual majority voted card
			msg.GetPlayerUpdate().Card = g.MajorityVotedCard

			// add to joined players set TODO: switch to map[string]bool
			for _, p := range g.MajorityJoinedPlayers {
				if p == msg.UserID {
					return true
				}
			}
			g.MajorityJoinedPlayers = append(g.MajorityJoinedPlayers, msg.UserID)
			return true
		}
	}
	return false
}

func (g *Server) majorityVoteExists() bool {
	if g.State == messaging.GameStatus_FINISHED && g.MajorityVotedCard != messaging.CardDeckCards_SPECIAL_NOMAJORITY {
		return true
	}
	return false
}

// UpdateGameStatus calculates the status of a game
func (g *Server) UpdateGameStatus(playerJoinedMajority bool) {
	if g.State == messaging.GameStatus_RUNNING || playerJoinedMajority {
		for _, player := range g.Players {
			if player.Card == messaging.CardDeckCards_SPECIAL_NOTCHOSEN && !player.IsHost {
				g.HighlightedPlayers = []string{}
				g.UnanimousVote = false
				return
			}
		}
		g.State = messaging.GameStatus_FINISHED
		g.HighlightedPlayers = g.determineHighlightedPlayersAndMajority()
	}
}

// determineHighlightedPlayersAndMajority returns a map containing all highlighted players and computes the majority vote
func (g *Server) determineHighlightedPlayersAndMajority() []string {
	playedCards := []float64{}

	for _, card := range g.GetPlayerStatus() {
		playedCards = append(playedCards, float64(card))
	}

	// determine if it was an unanimous vote based on the variance
	variance, err := stats.Variance(playedCards)
	if err != nil {
		g.Log.Error(err)
		return []string{}
	}
	if variance == 0 {
		g.UnanimousVote = true
		return []string{}
	}
	g.UnanimousVote = false

	// determine players to highlight
	min, err := stats.Min(playedCards)
	if err != nil {
		g.Log.Error(err)
		return []string{}
	}

	max, err := stats.Max(playedCards)
	if err != nil {
		g.Log.Error(err)
		return []string{}
	}

	mode, err := stats.Mode(playedCards)
	if err != nil {
		g.Log.Error(err)
		return []string{}
	}

	cardsToHighlight := map[float64]bool{}

	if len(mode) > 0 {
		g.MajorityVotedCard = pluralityFromModesAndPlayedCards(mode, playedCards)

		if min != mode[0] {
			cardsToHighlight[min] = true
		}
		if max != mode[0] {
			cardsToHighlight[max] = true
		}
	} else {
		g.MajorityVotedCard = messaging.CardDeckCards_SPECIAL_NOMAJORITY
		cardsToHighlight[min] = true
		cardsToHighlight[max] = true
	}

	highlightedPlayers := []string{}

	for _, p := range g.Players {
		if p.IsHost {
			continue
		}

		for c := range cardsToHighlight {
			if float64(p.Card) == c {
				highlightedPlayers = append(highlightedPlayers, p.ID)
				continue
			}
		}
	}

	g.Log.Infof("calculated highlightedPlayers: %s", highlightedPlayers)
	return highlightedPlayers
}

// pluralityFromModesAndPlayedCards determines the card which was voted for the most by the modes and playedCards
func pluralityFromModesAndPlayedCards(modes []float64, playedCards []float64) messaging.CardDeckCards {

	if len(modes) == 0 {
		return messaging.CardDeckCards_SPECIAL_NOMAJORITY
	}

	if len(modes) == 1 {
		return messaging.CardDeckCards(modes[0])
	}

	cFirstMode := 0
	cSecondMode := 0

	for _, c := range playedCards {
		if c == modes[0] {
			cFirstMode++
		}

		if c == modes[1] {
			cSecondMode++
		}
	}

	if cFirstMode > cSecondMode {
		return messaging.CardDeckCards(modes[0])
	}

	if cSecondMode > cFirstMode {
		return messaging.CardDeckCards(modes[1])
	}

	return messaging.CardDeckCards_SPECIAL_NOMAJORITY
}

// ChangeTopic changes the topic of a game and starts a fresh round
func (g *Server) ChangeTopic(newTopic string, newTopicLink string) {
	g.Topic = newTopic
	g.TopicLink = sanitizeTopicLink(newTopicLink)
	g.Round = 1
}

func sanitizeTopicLink(topicLink string) string {
	u, err := url.Parse(topicLink)

	if err != nil || u.Scheme == "" || u.Host == "" {
		return blankLink
	}

	return topicLink
}

// ResetPlayerCards sets all player cards to the not chosen state
func (g *Server) ResetPlayerCards() {
	for _, p := range g.Players {
		p.Card = messaging.CardDeckCards_SPECIAL_NOTCHOSEN
	}
	g.MajorityJoinedPlayers = []string{}
}

// BroadcastAndPersistStatusUpdate broadcasts a status update
func (g *Server) BroadcastAndPersistStatusUpdate() {

	msg := messaging.Container{
		GameID: g.ID,
		Msg: &messaging.Container_GameStatusUpdate{
			GameStatusUpdate: &messaging.GameStatusUpdate{
				PlayerStatus:          g.GetPlayerStatus(),
				HighlightedPlayers:    g.HighlightedPlayers,
				MajorityJoinedPlayers: g.MajorityJoinedPlayers,
				Hosts:                 g.GetHostIDs(),
				GameStatus:            g.State,
				GameTitle:             g.Title,
				CardDeck:              g.CardDeck,
				GameTopic:             g.Topic,
				GameTopicLink:         g.TopicLink,
				GameRound:             g.Round,
				UnanimousVote:         g.UnanimousVote,
				MajorityVotedCard:     g.MajorityVotedCard,
			},
		},
	}

	g.LastUpdatedTS = time.Now()

	err := g.PersistGameState()
	if err != nil {
		g.Log.Errorf("Error persisting game state: %s", err)
	}

	g.OutgoingMessage <- &msg
}

// BroadcastKickUser broadcasts a kicked user event to the gateway
func (g *Server) BroadcastKickUser(userID string) {

	msg := messaging.Container{
		GameID: g.ID,

		Msg: &messaging.Container_KickUser{
			KickUser: &messaging.KickUser{
				UserID: userID,
			},
		},
	}

	g.OutgoingMessage <- &msg
}

//TODO: Track hosts separately in GameServer

// GetHostIDs returns all IDs of a host
func (g *Server) GetHostIDs() []string {
	hosts := []string{}
	for _, p := range g.Players {
		if p.IsHost {
			hosts = append(hosts, p.ID)
		}
	}
	return hosts
}

// GetPlayerStatus returns a map containing all player states
func (g *Server) GetPlayerStatus() map[string]messaging.CardDeckCards {
	playerStatus := map[string]messaging.CardDeckCards{}

	for _, p := range g.Players {
		if !p.IsHost {

			switch g.State {
			case messaging.GameStatus_RUNNING:
				// return masked cards if game is still running
				if p.Card == messaging.CardDeckCards_SPECIAL_NOTCHOSEN {
					playerStatus[p.ID] = messaging.CardDeckCards_SPECIAL_NOTCHOSEN
				} else {
					playerStatus[p.ID] = messaging.CardDeckCards_SPECIAL_CHOSEN
				}
			case messaging.GameStatus_FINISHED:
				// return actual cards if game is finished
				playerStatus[p.ID] = p.Card
			default:
				// otherwise return masked card so we get the joined players
				playerStatus[p.ID] = messaging.CardDeckCards_SPECIAL_NOTCHOSEN
			}

		}
	}
	return playerStatus
}

// isValidCard determines whether the player card is valid for the chosen card deck
func isValidCard(card messaging.CardDeckCards, cardDeck messaging.CardDeckType) bool {
	var validCards []messaging.CardDeckCards

	switch cardDeck {
	case messaging.CardDeckType_DEFAULT:
		validCards = []messaging.CardDeckCards{
			messaging.CardDeckCards_DEFAULT_ZERO,
			messaging.CardDeckCards_DEFAULT_PFIVE,
			messaging.CardDeckCards_DEFAULT_ONE,
			messaging.CardDeckCards_DEFAULT_TWO,
			messaging.CardDeckCards_DEFAULT_THREE,
			messaging.CardDeckCards_DEFAULT_FIVE,
			messaging.CardDeckCards_DEFAULT_EIGHT,
			messaging.CardDeckCards_DEFAULT_THIRTEEN,
			messaging.CardDeckCards_DEFAULT_TWENTY,
			messaging.CardDeckCards_DEFAULT_HUNDRED,
		}

	case messaging.CardDeckType_TSHIRT:
		validCards = []messaging.CardDeckCards{
			messaging.CardDeckCards_TSHIRT_S,
			messaging.CardDeckCards_TSHIRT_M,
			messaging.CardDeckCards_TSHIRT_L,
			messaging.CardDeckCards_TSHIRT_XL,
		}

	default:
		return false
	}

	for _, c := range validCards {
		if card == c {
			return true
		}
	}
	return false
}

// Shutdown triggers the shutdown of the game server
func (g *Server) Shutdown(reason string) {
	g.Log.Infof("Shutting down with reason: %s", reason)

	// get all current hosts so we know how many will disconnect
	hosts, err := g.RedisClient.GetGameHostsForGame(g.ID)
	if err != nil {
		g.Log.Errorf("Error getting all hosts: %s", err)
	} else {
		g.metricsCollector.DPActiveUsersTotal.With(prometheus.Labels{instrumentation.LabelIsHost: "true"}).Sub(float64(len(hosts)))
	}

	err = g.RedisClient.RemoveAllGameHostsForGame(g.ID)
	if err != nil {
		g.Log.Errorf("Error removing all hosts: %s", err)
	}

	// get all current players so we know how many will disconnect
	players, err := g.RedisClient.GetGamePlayersForGame(g.ID)
	if err != nil {
		g.Log.Errorf("Error getting all players: %s", err)
	} else {
		g.metricsCollector.DPActiveUsersTotal.With(prometheus.Labels{instrumentation.LabelIsHost: "false"}).Sub(float64(len(players)))
	}

	err = g.RedisClient.RemoveAllGamePlayersForGame(g.ID)
	if err != nil {
		g.Log.Errorf("Error removing all players: %s", err)
	}

	err = g.RedisClient.RemoveGameStateForGame(g.ID)
	if err != nil {
		g.Log.Errorf("Error removing game state: %s", err)
	}

	err = g.invalidateAllTokens()
	if err != nil {
		g.Log.Errorf("Error invalidating all tokens: %s", err)
	}

	g.shutdown <- true
}
