package instrumentation

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	muxprom "gitlab.com/msvechla/mux-prometheus/pkg/middleware"
)

const (
	namespace           = "gio"
	gameserverSubsystem = "gameserver"
	dispatcherSubsystem = "dispatcher"

	// LabelDispatcherID is the label for the dispatcher id
	LabelDispatcherID = "dispatcher_id"
	// LabelMessageType is the label for the message type
	LabelMessageType = "message_type"
	// LabelIsHost os the label indicating whether the metric is host related
	LabelIsHost = "is_host"
)

var (
	// DPMessageTypePlayerUpdate type player update
	DPMessageTypePlayerUpdate = prometheus.Labels{"message_type": "playerupdate"}
	// DPMessageTypeHostUpdate type for host update
	DPMessageTypeHostUpdate = prometheus.Labels{"message_type": "hostupdate"}
	// DPMessageTypeKickUser type for kick user
	DPMessageTypeKickUser = prometheus.Labels{"message_type": "kickuser"}
	// DPMessageTypeConnection type for connections
	DPMessageTypeConnection = prometheus.Labels{"message_type": "connection"}
	// DPMessageTypeUndefined type for undefined messages
	DPMessageTypeUndefined = prometheus.Labels{"message_type": "undefined"}
)

// MetricsCollector contains all data that will be collected
type MetricsCollector struct {
	registerer              prometheus.Registerer
	MuxProm                 *muxprom.Instrumentation
	DPActiveGamesTotal      prometheus.Gauge
	DPActiveUsersTotal      *prometheus.GaugeVec
	DPMessagesReceivedTotal *prometheus.CounterVec
	DPMessagesSentTotal     *prometheus.CounterVec
}

// NewMetricsCollector creates a new metrics collector and registers all metrics
func NewMetricsCollector(dispatcherID string) *MetricsCollector {
	mc := MetricsCollector{
		DPActiveGamesTotal: prometheus.NewGauge(prometheus.GaugeOpts{
			Name:      "active_games_total",
			Subsystem: dispatcherSubsystem,
			Namespace: namespace,
			Help:      "The total number of current active games",
		}),
		DPActiveUsersTotal: prometheus.NewGaugeVec(prometheus.GaugeOpts{
			Name:      "active_players_total",
			Subsystem: dispatcherSubsystem,
			Namespace: namespace,
			Help:      "The total number of current active players",
		}, []string{LabelIsHost}),
		DPMessagesReceivedTotal: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:      "messages_received_total",
			Subsystem: dispatcherSubsystem,
			Namespace: namespace,
			Help:      "The total number of messages received",
		}, []string{LabelMessageType}),
		DPMessagesSentTotal: prometheus.NewCounterVec(prometheus.CounterOpts{
			Name:      "messages_sent_total",
			Subsystem: dispatcherSubsystem,
			Namespace: namespace,
			Help:      "The total number of messages sent",
		}, []string{LabelMessageType}),
		registerer: prometheus.WrapRegistererWith(prometheus.Labels{LabelDispatcherID: dispatcherID}, prometheus.DefaultRegisterer),
	}
	mc.registerer.MustRegister(
		mc.DPActiveUsersTotal,
		mc.DPActiveGamesTotal,
		mc.DPMessagesReceivedTotal,
		mc.DPMessagesSentTotal,
	)

	return &mc
}

// ServeMetrics serves the prometheus metrics so they can get scraped
func (m *MetricsCollector) ServeMetrics() {
	r := mux.NewRouter()

	m.MuxProm = muxprom.NewCustomInstrumentation(true, "mux", "router", prometheus.DefBuckets, map[string]string{}, m.registerer)
	r.Use(m.MuxProm.Middleware)

	r.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":9102", r))
}
