module gitlab.com/msvechla/rio-gameserver

go 1.13

require (
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/golang/protobuf v1.5.2
	github.com/google/uuid v1.3.0
	github.com/gorilla/mux v1.8.0
	github.com/montanaflynn/stats v0.6.6
	github.com/onsi/ginkgo v1.11.0 // indirect
	github.com/onsi/gomega v1.8.1 // indirect
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.13.0
	github.com/sirupsen/logrus v1.9.0
	github.com/streadway/amqp v1.0.0
	github.com/urfave/cli/v2 v2.11.2
	gitlab.com/msvechla/mux-prometheus v0.0.2
	golang.org/x/sys v0.0.0-20220825204002-c680a09ffe64 // indirect
	google.golang.org/protobuf v1.28.1
)
