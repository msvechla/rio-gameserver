# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.1.0] - 2020-11-10

### Added

- option to specify link to game topics

## [2.0.0] - 2020-10-27

### Added

- switched from json to protobuf api

## [1.0.0] - 2020-04-09

### Added

- initial version

