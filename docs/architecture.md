# Architecture

This is an overview of the [refinable.io](https://refinable.io) microservice architecture:

![highlevel architecture](highlevel_architecture.png)

> ⚠️  keep in mind that this architecture was for educational purposes on how to implementa scalable, stateful game-server architecture.
> There are more efficient architecture approaches for this fairly simple application.

## Design Goals

- high availability and scalability for components with lots of traffic
- real-time communication and state updates including the frontend
- optimized for short-lived game sessions
- stateful game-server, which has all information available in-memory to make deicions
- vertical (number of managed sessions per gameserver) and horizontal scaling for the game-server
- all components should scale horizontally

## Components

### Microservices

- [rio-frontend](https://gitlab.com/msvechla/rio-frontend) - the vuejs frontend
- [rio-gateway](https://gitlab.com/msvechla/rio-gateway) - the main user-facing gateway that handles websocket connections
- [rio-session](https://gitlab.com/msvechla/rio-sessions) - microservice for issuing session tokes and creating/distributing games
- [rio-gameserver](https://gitlab.com/msvechla/rio-gameserver) - responsible for the main refining session logic

### Infrastructure

- frontend served as static content behind a CDN
- microservice stack deployed to a Kubernetes cluster
  - rio-gameserver as stateful set
  - all other components as deployments
- redis-cluster inside Kubernetes used as temporary in-memory key / value store
- rabbitmq inside Kubernetes used for asynchronous messaging / decoupling of microservices
