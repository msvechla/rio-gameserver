package redis

import (
	"fmt"

	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

// Client represents a redis client
type Client struct {
	Universal redis.UniversalClient
}

// ErrNoActiveGameServer represents an error when no game serer is currently active
type ErrNoActiveGameServer struct{}

func (e *ErrNoActiveGameServer) Error() string {
	return "No gameserver with active games found"
}

// NewClient creates a new redis client
func NewClient(masterAddr string, password string) (*Client, error) {
	rdb := redis.NewUniversalClient(&redis.UniversalOptions{
		Addrs:    []string{masterAddr},
		Password: password,
	})

	ping := rdb.Ping()
	if ping.Err() != nil {
		return nil, errors.Wrap(ping.Err(), "Error initially pinging redis instance")
	}
	return &Client{Universal: rdb}, nil
}

// InitActiveGamesForGameServer adds the dispatcher to the set of activeGames and initializes it with score 0 if it does not exist yet
func (c *Client) InitActiveGamesForGameServer(dispatcherID string) error {
	result := c.Universal.ZAddNX(KeyGameServerActiveGames, redis.Z{Score: 0, Member: dispatcherID})
	return result.Err()
}

// IncrementActiveGamesForGameServerByAmount increases the active games for the prprovided gameserver by amount and returns the new score
func (c *Client) IncrementActiveGamesForGameServerByAmount(dispatcherID string, incrementBy float64) (float64, error) {
	result := c.Universal.ZIncrBy(KeyGameServerActiveGames, incrementBy, dispatcherID)
	return result.Val(), result.Err()
}

// GetGameServerWithLeastActiveGames returns the game server with the least amount of active games
func (c *Client) GetGameServerWithLeastActiveGames() (string, error) {
	result := c.Universal.ZRange(KeyGameServerActiveGames, 0, 0)
	if len(result.Val()) == 0 {
		return "", &ErrNoActiveGameServer{}
	}
	return result.Val()[0], result.Err()
}

// SetGameStateForGame persists the flat game state to redis
func (c *Client) SetGameStateForGame(gameID string, state map[string]interface{}) error {
	result := c.Universal.HMSet(fmt.Sprintf(FormatKeySetGameStateForGameID, gameID), state)
	return result.Err()
}

// ExistsGame checks whether a game with the given gameID exists
func (c *Client) ExistsGame(gameID string) (bool, error) {
	result := c.Universal.Exists(fmt.Sprintf(FormatKeySetGameStateForGameID, gameID))
	if result.Err() != nil {
		return false, result.Err()
	}
	return result.Val() == 1, nil
}

// GetGameStateForGame returns the flat game state from redis
func (c *Client) GetGameStateForGame(gameID string) (map[string]string, error) {
	result := c.Universal.HGetAll(fmt.Sprintf(FormatKeySetGameStateForGameID, gameID))
	return result.Result()
}

// RemoveGameStateForGame removes the game state from redis
func (c *Client) RemoveGameStateForGame(gameID string) error {
	result := c.Universal.Del(fmt.Sprintf(FormatKeySetGameStateForGameID, gameID))
	return result.Err()
}

// AddGamePlayerForGame persists a player of a game
func (c *Client) AddGamePlayerForGame(gameID string, player string) error {
	result := c.Universal.SAdd(fmt.Sprintf(FormatKeySetGamePlayersForGameID, gameID), player)
	return result.Err()
}

// RemoveGamePlayerForGame removes the player from a game
func (c *Client) RemoveGamePlayerForGame(gameID string, player string) error {
	result := c.Universal.SRem(fmt.Sprintf(FormatKeySetGamePlayersForGameID, gameID), player)
	return result.Err()
}

// RemoveAllGamePlayersForGame removes all players of a game
func (c *Client) RemoveAllGamePlayersForGame(gameID string) error {
	result := c.Universal.Del(fmt.Sprintf(FormatKeySetGamePlayersForGameID, gameID))
	return result.Err()
}

// GetGamePlayersForGame returns the players of a game
func (c *Client) GetGamePlayersForGame(gameID string) ([]string, error) {
	result := c.Universal.SMembers(fmt.Sprintf(FormatKeySetGamePlayersForGameID, gameID))
	return result.Result()
}

// AddGameHostForGame persists a host of a game
func (c *Client) AddGameHostForGame(gameID string, host string) error {
	result := c.Universal.SAdd(fmt.Sprintf(FormatKeySetGameHostsForGameID, gameID), host)
	return result.Err()
}

// RemoveGameHostForGame removes a host of a game from persistence
func (c *Client) RemoveGameHostForGame(gameID string, host string) error {
	result := c.Universal.SRem(fmt.Sprintf(FormatKeySetGameHostsForGameID, gameID), host)
	return result.Err()
}

// RemoveAllGameHostsForGame removes all host of a game from persistence
func (c *Client) RemoveAllGameHostsForGame(gameID string) error {
	result := c.Universal.Del(fmt.Sprintf(FormatKeySetGameHostsForGameID, gameID))
	return result.Err()
}

// GetGameHostsForGame returns the players of a game
func (c *Client) GetGameHostsForGame(gameID string) ([]string, error) {
	result := c.Universal.SMembers(fmt.Sprintf(FormatKeySetGameHostsForGameID, gameID))
	return result.Result()
}

// AddGameServerHostingGamesForDispatcher persists a game which a dispatcher is responsible for
func (c *Client) AddGameServerHostingGamesForDispatcher(dispatcherID string, gameID string) error {
	result := c.Universal.SAdd(fmt.Sprintf(FormatKeySetGameServerHostingGamesForDispatcher, dispatcherID), gameID)
	return result.Err()
}

// RemoveGameServerHostingGamesForDispatcher removes a game which a dispatcher is responsible for
func (c *Client) RemoveGameServerHostingGamesForDispatcher(dispatcherID string, gameID string) error {
	result := c.Universal.SRem(fmt.Sprintf(FormatKeySetGameServerHostingGamesForDispatcher, dispatcherID), gameID)
	return result.Err()
}

// GetGameServerHostingGamesForDispatcher gets all games which a dispatcher is responsible for
func (c *Client) GetGameServerHostingGamesForDispatcher(dispatcherID string) ([]string, error) {
	result := c.Universal.SMembers(fmt.Sprintf(FormatKeySetGameServerHostingGamesForDispatcher, dispatcherID))
	return result.Result()
}

// GetGameTokenForUserInGameID gets the token for a user in a specific game
func (c *Client) GetGameTokenForUserInGameID(userID string, gameID string) (string, error) {
	result := c.Universal.HGet(fmt.Sprintf(FormatKeyHashGameTokensForGameID, gameID), userID)
	return result.Result()
}

// SetNXGameTokenForUserInGameID sets a token of a user in a game if a token does not already exist for the user
func (c *Client) SetNXGameTokenForUserInGameID(userID string, gameID string, token string) (bool, error) {
	result := c.Universal.HSetNX(fmt.Sprintf(FormatKeyHashGameTokensForGameID, gameID), userID, token)
	return result.Result()
}

// RemoveGameTokenForUserInGameID removes a token of a user in a game
func (c *Client) RemoveGameTokenForUserInGameID(userID string, gameID string) error {
	result := c.Universal.HDel(fmt.Sprintf(FormatKeyHashGameTokensForGameID, gameID), userID)
	return result.Err()
}

// RemoveAllGameTokensForGameID removes all tokens for a game
func (c *Client) RemoveAllGameTokensForGameID(gameID string) error {
	result := c.Universal.Del(fmt.Sprintf(FormatKeyHashGameTokensForGameID, gameID))
	return result.Err()
}
