package redis

const (
	// KeyGameServerActiveGames is the hashset containing the gameserverID with a score of current active games
	KeyGameServerActiveGames = "GameServer.ActiveGames"
	// FormatKeySetGameServerHostingGamesForDispatcher is the set of gameIDs a server is responsible for
	FormatKeySetGameServerHostingGamesForDispatcher = "GameServer.HostingGames:%s"
	// FormatKeySetGameStateForGameID contains the flat game state of a game
	FormatKeySetGameStateForGameID = "Game.State:%s"
	// FormatKeySetGamePlayersForGameID contains the set of all players of a game
	FormatKeySetGamePlayersForGameID = "Game.Players:%s"
	// FormatKeySetGameHostsForGameID contains the set of all hosts of a game
	FormatKeySetGameHostsForGameID = "Game.Hosts:%s"
	// FormatKeyHashGameTokensForGameID contains the user tokens for a game
	FormatKeyHashGameTokensForGameID = "Game.Tokens:%s"
)
